# Information and communication -- (J)

## Cluster - 7 (J-HIC)
Athens (GRC); Leeds (GBR); London (GBR); Manchester (GBR); Milan (ITA)

[![https://upload.wikimedia.org/wikipedia/commons/5/58/Milan.Proper.Wikipedia.Image.png](https://upload.wikimedia.org/wikipedia/commons/5/58/Milan.Proper.Wikipedia.Image.png)](https://upload.wikimedia.org/wikipedia/commons/5/58/Milan.Proper.Wikipedia.Image.png)
[![https://upload.wikimedia.org/wikipedia/commons/c/c5/Manchester_Montage_2017.jpg](https://upload.wikimedia.org/wikipedia/commons/c/c5/Manchester_Montage_2017.jpg)](https://upload.wikimedia.org/wikipedia/commons/c/c5/Manchester_Montage_2017.jpg)


**Table 17**: Desription of cluster 7

|       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|----------:|----------------:|----------:|-----------:|----------------:|
| count | 5         |      5          |      5    |       5    |        5        |
| mean  | 0.0795892 |      0.0722178  |   5425.81 |   23525.9  |        1.54107  |
| std   | 0.0179927 |      0.00401326 |   3110.83 |    5925.03 |        0.807151 |
| min   | 0.0661607 |      0.0650483  |   2204    |   15761.6  |        0.796458 |
| 25%   | 0.0671607 |      0.0737609  |   3198.44 |   20302.2  |        0.828145 |
| 50%   | 0.0748537 |      0.0738652  |   5210.12 |   24234.8  |        1.40225  |
| 75%   | 0.0795595 |      0.0741271  |   6350.92 |   25800.7  |        1.98582  |
| max   | 0.110212  |      0.0742874  |  10165.6  |   31530.2  |        2.69267  |

**Table 18**: Cities of cluster 7

| city       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-----------|----------:|----------------:|----------:|-----------:|----------------:|
| Athens     | 0.110212  |       0.0650483 |  10165.6  |    20302.2 |        0.796458 |
| Leeds      | 0.0671607 |       0.0737609 |   5210.12 |    15761.6 |        1.40225  |
| London     | 0.0748537 |       0.0742874 |   3198.44 |    25800.7 |        2.69267  |
| Manchester | 0.0661607 |       0.0738652 |   2204    |    31530.2 |        1.98582  |
| Milan      | 0.0795595 |       0.0741271 |   6350.92 |    24234.8 |        0.828145 |

## Cluster - 8 (J-UMIC)
Ambato (ECU); Bahía Blanca (ARG); Cancún (MEX); Cartagena (COL); Chihuahua (MEX); Esmeraldas (ECU); Formosa (ARG); Guayaquil (ECU); Latacunga (ECU); Machala (ECU); Mexico City (MEX); Portoviejo (ECU); Posadas (ARG); Quito (ECU); Recife (BRA); Riobamba (ECU); Rosario (ARG); Santa Elena (ECU); Santa Fe (ARG); Tulcán (ECU); Zamora (ECU)

[![https://upload.wikimedia.org/wikipedia/commons/b/b9/Recife_-_In%C3%ADcio_da_Avenida_Boa_Viagem.jpg](https://upload.wikimedia.org/wikipedia/commons/b/b9/Recife_-_In%C3%ADcio_da_Avenida_Boa_Viagem.jpg)](https://upload.wikimedia.org/wikipedia/commons/b/b9/Recife_-_In%C3%ADcio_da_Avenida_Boa_Viagem.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/e/ed/Banner_Riobamba_Ciudad_Bonita.jpg](https://upload.wikimedia.org/wikipedia/commons/e/ed/Banner_Riobamba_Ciudad_Bonita.jpg)](https://upload.wikimedia.org/wikipedia/commons/e/ed/Banner_Riobamba_Ciudad_Bonita.jpg)


**Table 19**: Desription of cluster 8

|       |          ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|------------:|----------------:|----------:|-----------:|----------------:|
| count | 21          |      21         |     21    |      21    |      21         |
| mean  |  0.0289574  |       0.122328  |  31329.8  |    8486.14 |       0.370861  |
| std   |  0.0264623  |       0.0687188 |  41458.9  |    4528.16 |       0.326696  |
| min   |  0.00258669 |       0.0532976 |   6665.7  |    2017.1  |       0.0550945 |
| 25%   |  0.00842495 |       0.0753867 |   9433.73 |    5723.62 |       0.126147  |
| 50%   |  0.0178396  |       0.0958577 |  15587.5  |    7398.41 |       0.283461  |
| 75%   |  0.0444742  |       0.139065  |  21018.9  |   11416.5  |       0.409171  |
| max   |  0.0878757  |       0.310538  | 179731    |   18488.2  |       1.427     |

**Table 20**: Cities of cluster 8

| city         |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-------------|-----------:|----------------:|----------:|-----------:|----------------:|
| Ambato       | 0.0124075  |       0.0958577 |  89562.4  |    5723.62 |       0.126147  |
| Bahía Blanca | 0.0712905  |       0.0683285 |   6665.7  |   13122.3  |       1.427     |
| Cancún       | 0.0444742  |       0.310538  |  11525.1  |   15456.8  |       0.35862   |
| Cartagena    | 0.0522151  |       0.280535  |  16685.5  |    7756.11 |       0.872427  |
| Chihuahua    | 0.0878757  |       0.0532976 |   7207.98 |   11416.5  |       0.319794  |
| Esmeraldas   | 0.00258669 |       0.115962  |  55245.7  |    2772.4  |       0.538527  |
| Formosa      | 0.0227333  |       0.125533  |   7936.77 |    5477.31 |       0.265319  |
| Guayaquil    | 0.00678508 |       0.143677  |  19603.2  |    9829.38 |       0.279029  |
| Latacunga    | 0.00492691 |       0.0706957 |  77920.4  |    2825.63 |       0.0668846 |
| Machala      | 0.00839389 |       0.139065  |  16290.8  |    7988.43 |       0.100389  |
| Mexico City  | 0.0660398  |       0.0803504 |  15066.6  |   18488.2  |       0.714246  |
| Portoviejo   | 0.00630281 |       0.0892703 |  21018.9  |    5739.98 |       0.120362  |
| Posadas      | 0.0398319  |       0.151222  |   8496.71 |    5904.74 |       0.376754  |
| Quito        | 0.020881   |       0.0640326 |  15587.5  |   10006.1  |       0.283461  |
| Recife       | 0.0713177  |       0.214392  |  18079.4  |    7398.41 |       0.347071  |
| Riobamba     | 0.0109255  |       0.0924321 | 179731    |    4647.07 |       0.0906245 |
| Rosario      | 0.0178396  |       0.103485  |   7572.45 |   14052.8  |       0.625765  |
| Santa Elena  | 0.0101218  |       0.126776  |   9433.73 |    6105.23 |       0.247909  |
| Santa Fe     | 0.0326203  |       0.0743075 |  10350.8  |   14367.5  |       0.409171  |
| Tulcán       | 0.00842495 |       0.0937513 |  50841.4  |    2017.1  |       0.0550945 |
| Zamora       | 0.0101105  |       0.0753867 |  13103.9  |    7113.45 |       0.163472  |

## Cluster - 9 (J-LMIC)
Chennai (IND); Kigali (RWA); Nairobi (KEN)

[![https://upload.wikimedia.org/wikipedia/commons/8/83/Kigali2018Cropped.jpg](https://upload.wikimedia.org/wikipedia/commons/8/83/Kigali2018Cropped.jpg)](https://upload.wikimedia.org/wikipedia/commons/8/83/Kigali2018Cropped.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/4/42/Nairobi_Montage_A.jpg](https://upload.wikimedia.org/wikipedia/commons/4/42/Nairobi_Montage_A.jpg)](https://upload.wikimedia.org/wikipedia/commons/4/42/Nairobi_Montage_A.jpg)


**Table 21**: Desription of cluster 9

|       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|----------:|----------------:|----------:|-----------:|----------------:|
| count | 3         |       3         |       3   |       3    |       3         |
| mean  | 0.0836621 |       0.0754784 |   33066.1 |    5169    |       0.275134  |
| std   | 0.0156293 |       0.0342046 |   20057.8 |    3478.68 |       0.226195  |
| min   | 0.0684009 |       0.0362593 |   15079.2 |    1341.14 |       0.0146703 |
| 25%   | 0.0756756 |       0.0636518 |   22251.3 |    3684.82 |       0.201609  |
| 50%   | 0.0829504 |       0.0910443 |   29423.5 |    6028.49 |       0.388549  |
| 75%   | 0.0912927 |       0.095088  |   42059.5 |    7082.93 |       0.405367  |
| max   | 0.0996351 |       0.0991316 |   54695.5 |    8137.37 |       0.422185  |

**Table 22**: Cities of cluster 9

| city    |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:--------|----------:|----------------:|----------:|-----------:|----------------:|
| Chennai | 0.0684009 |       0.0362593 |   29423.5 |    6028.49 |       0.422185  |
| Kigali  | 0.0996351 |       0.0991316 |   15079.2 |    1341.14 |       0.0146703 |
| Nairobi | 0.0829504 |       0.0910443 |   54695.5 |    8137.37 |       0.388549  |

