# Administration and education -- (NMP)

## Cluster - 13 (NMP-HIC)
Copenhagen (DNK); Lisbon (PRT); Liverpool (GBR); Thessaloniki (GRC)

[![https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Thessalonica_Montage_L.png/275px-Thessalonica_Montage_L.png](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Thessalonica_Montage_L.png/275px-Thessalonica_Montage_L.png)](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Thessalonica_Montage_L.png/275px-Thessalonica_Montage_L.png)
[![https://upload.wikimedia.org/wikipedia/commons/1/11/Nyhavn-panorama.jpg](https://upload.wikimedia.org/wikipedia/commons/1/11/Nyhavn-panorama.jpg)](https://upload.wikimedia.org/wikipedia/commons/1/11/Nyhavn-panorama.jpg)


**Table 27**: Desription of cluster 13

|       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|----------:|----------------:|----------:|-----------:|----------------:|
| count | 4         |       4         |      4    |       4    |        4        |
| mean  | 0.0921347 |       0.0908593 |   7431.42 |   21583.2  |        1.17352  |
| std   | 0.0249632 |       0.0105227 |   2845.51 |    9656.12 |        0.343461 |
| min   | 0.0646479 |       0.0752936 |   5012.62 |   12067.3  |        0.917842 |
| 25%   | 0.077702  |       0.0890186 |   5165.41 |   15090.9  |        0.92207  |
| 50%   | 0.0902951 |       0.095328  |   6891.05 |   20177.9  |        1.06417  |
| 75%   | 0.104728  |       0.0971686 |   9157.07 |   26670.1  |        1.31561  |
| max   | 0.123301  |       0.0974875 |  10931    |   33909.7  |        1.64789  |

**Table 28**: Cities of cluster 13

| city         |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-------------|----------:|----------------:|----------:|-----------:|----------------:|
| Copenhagen   | 0.123301  |       0.0974875 |   5012.62 |    33909.7 |        0.923479 |
| Lisbon       | 0.0820533 |       0.0970624 |   8565.77 |    24256.9 |        1.20486  |
| Liverpool    | 0.0646479 |       0.0935936 |   5216.34 |    16098.8 |        1.64789  |
| Thessaloniki | 0.0985369 |       0.0752936 |  10931    |    12067.3 |        0.917842 |

## Cluster - 14 (NMP-UMIC)
Concordia (ARG); Corrientes (ARG); La Paz (BOL); La Rioja (ARG); Manizales (COL); Resistencia (ARG); Reynosa (MEX); Veracruz (MEX); Viedma (ARG)

[![https://upload.wikimedia.org/wikipedia/commons/e/eb/Manizales_Montage_L.jpg](https://upload.wikimedia.org/wikipedia/commons/e/eb/Manizales_Montage_L.jpg)](https://upload.wikimedia.org/wikipedia/commons/e/eb/Manizales_Montage_L.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/a/a4/La_Paz_Skyline.jpg](https://upload.wikimedia.org/wikipedia/commons/a/a4/La_Paz_Skyline.jpg)](https://upload.wikimedia.org/wikipedia/commons/a/a4/La_Paz_Skyline.jpg)


**Table 29**: Desription of cluster 14

|       |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|-----------:|----------------:|----------:|-----------:|----------------:|
| count | 9          |       9         |      9    |       9    |       9         |
| mean  | 0.0415347  |       0.179437  |  11237.3  |    9147.52 |       0.344802  |
| std   | 0.0433031  |       0.176453  |   7843.78 |    2208.15 |       0.267429  |
| min   | 0.00921147 |       0.0648727 |   6293.84 |    6820.24 |       0.0737804 |
| 25%   | 0.0160986  |       0.0778829 |   7757.17 |    7685.15 |       0.145552  |
| 50%   | 0.0233472  |       0.128235  |   8459.19 |    8533.42 |       0.273969  |
| 75%   | 0.0518162  |       0.150956  |  10633.6  |   10471.3  |       0.448362  |
| max   | 0.143814   |       0.633169  |  31562    |   13923    |       0.793916  |

**Table 30**: Cities of cluster 14

| city        |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------------|-----------:|----------------:|----------:|-----------:|----------------:|
| Concordia   | 0.0164283  |       0.145931  |   7757.17 |    9021.79 |       0.147998  |
| Corrientes  | 0.0160986  |       0.150956  |  10633.6  |    7173.64 |       0.4361    |
| La Paz      | 0.143814   |       0.633169  |  12171    |   10471.3  |       0.145552  |
| La Rioja    | 0.0233472  |       0.128235  |   9449.28 |    8226.07 |       0.273969  |
| Manizales   | 0.0518162  |       0.0648727 |  31562    |    7685.15 |       0.0834903 |
| Resistencia | 0.00921147 |       0.121896  |   8122.66 |    6820.24 |       0.793916  |
| Reynosa     | 0.0114033  |       0.0778829 |   6293.84 |   13923    |       0.448362  |
| Veracruz    | 0.0695582  |       0.0764575 |   8459.19 |    8533.42 |       0.700047  |
| Viedma      | 0.0321353  |       0.215535  |   6687.09 |   10473.1  |       0.0737804 |

## Cluster - 15 (NMP-LMIC)
Huambo (AGO); Kanpur (IND); Kolkata (IND); Lucknow (IND); Ludhiana (IND); Mumbai (IND); Nagpur (IND); Patna (IND); Pune (IND); Sialkot (PAK); Varanasi (IND)

[![https://upload.wikimedia.org/wikipedia/commons/d/df/South-mumbai-2020.jpg](https://upload.wikimedia.org/wikipedia/commons/d/df/South-mumbai-2020.jpg)](https://upload.wikimedia.org/wikipedia/commons/d/df/South-mumbai-2020.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/d/dc/Bab_ay_Sialkot.jpg](https://upload.wikimedia.org/wikipedia/commons/d/dc/Bab_ay_Sialkot.jpg)](https://upload.wikimedia.org/wikipedia/commons/d/dc/Bab_ay_Sialkot.jpg)


**Table 31**: Desription of cluster 15

|       |          ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|------------:|----------------:|----------:|-----------:|----------------:|
| count | 11          |      11         |      11   |      11    |       11        |
| mean  |  0.0808615  |       0.114329  |   35789.7 |    4362.33 |        0.261613 |
| std   |  0.0482338  |       0.0512286 |   15981.7 |    2116.76 |        0.123724 |
| min   |  0.00235242 |       0.0362132 |   14132   |    1760.06 |        0.12127  |
| 25%   |  0.0497013  |       0.0892944 |   22303.4 |    2565.35 |        0.164979 |
| 50%   |  0.0768856  |       0.10775   |   37058.7 |    4073.45 |        0.214053 |
| 75%   |  0.107471   |       0.121594  |   42930.9 |    6514.8  |        0.37063  |
| max   |  0.174076   |       0.213489  |   61056.1 |    7207.63 |        0.46854  |

**Table 32**: Cities of cluster 15

| city     |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:---------|-----------:|----------------:|----------:|-----------:|----------------:|
| Huambo   | 0.0311365  |       0.10775   |   17648.2 |    4422.12 |        0.128084 |
| Kanpur   | 0.128515   |       0.193357  |   37058.7 |    2458.59 |        0.213935 |
| Kolkata  | 0.0768856  |       0.107251  |   17747.4 |    4073.45 |        0.404644 |
| Lucknow  | 0.00235242 |       0.111185  |   44164.8 |    2380.02 |        0.336617 |
| Ludhiana | 0.0968448  |       0.100277  |   14132   |    6402.78 |        0.133835 |
| Mumbai   | 0.0868697  |       0.0783123 |   59741   |    6626.82 |        0.40796  |
| Nagpur   | 0.0486466  |       0.0666009 |   26859.4 |    7154.31 |        0.196123 |
| Patna    | 0.050756   |       0.127996  |   61056.1 |    1760.06 |        0.252682 |
| Pune     | 0.0752966  |       0.0362132 |   32919.1 |    7207.63 |        0.214053 |
| Sialkot  | 0.174076   |       0.115192  |   40663   |    2672.1  |        0.12127  |
| Varanasi | 0.118097   |       0.213489  |   41697   |    2827.77 |        0.46854  |

