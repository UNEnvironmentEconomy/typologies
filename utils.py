#!/usr/bin/env python
# coding: utf-8
# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# %%
usecol = [
    'city',
    'country_code',
    'ce',
    'hhi_circ_norm',
    'P15_B15',
    'GDP15_PP',
    'E_EC2E_I15_PP',
    'sec_main',
    'INCM_CMI',
]


# %%
sectors_dict = {
    'A': -1, 
    'B': 1,'C': 1,'D': -1,'E': -1,
    'F': 0.5,'G': 1,'H': 0.5,'I': -1,
    'J': 1,'K': -1,'L': -1,'M': 1,
    'N': 1,'P': 1,'R': -1,}


# %%
def get_data():

    cities = pd.read_csv("data_em/city_name_select_distinct.csv", usecols=["city_name", "ce"])

    usecols = [
        'UC_NM_MN', 'E_KG_NM_LST', 'B15', 'P15',
        'GDP15_SM', 'INCM_CMI', 'DEV_CMI', 'TT2CC', 'E_EC2E_I15', 'CTR_MN_NM']

    ghs = pd.read_csv(
        "data_em/ghs.csv", usecols=usecols,
        dtype={"E_KG_NM_LST":"category",
               "INCM_CMI":"category",
               "DEV_CMI":"category"})

    ghs.loc[:, 'B15_PP'] = ghs.B15.div(ghs.P15).mul(1e6)
    ghs.loc[:, 'GDP15_PP'] = ghs.GDP15_SM.div(ghs.P15)
    ghs.loc[:, 'P15_B15'] = ghs.P15.div(ghs.B15)
    ghs.loc[:, 'E_EC2E_I15_PP'] = ghs.E_EC2E_I15.div(ghs.P15)

    diversity = pd.read_stata('data_em/cities_diversity_measures.dta')
    ranking_raw = pd.read_stata('data_em/cities_sectoral_rankings.dta') 

    ranking = pd.DataFrame(
        columns=[
            'city', 'country_code',
            'ranking_sec_a', 'ranking_sec_b', 'ranking_sec_c',
            'ranking_a', 'ranking_b', 'ranking_c',
        ])

    tmp_index = ['city', 'country_code', 'year', 
                 'ranking_sec_a', 'ranking_sec_b', 'ranking_sec_c',
                 'ranking_a', 'ranking_b', 'ranking_c']

    for e, city in enumerate(ranking_raw.city_name.unique()):
        tmp = ranking_raw.loc[ranking_raw.city_name == city]
        tmp = tmp.iloc[0, 0:3].append(tmp.loc[:, 'alpha_desc']).append(tmp.loc[:, 'index'])
        tmp.index = tmp_index
        ranking.loc[e] = tmp

    ranking.loc[:, 'sec_a'] = ranking.ranking_sec_a.apply(lambda x: x.split(':')[0].split(',')[0].split('&')[0].strip())
    ranking.loc[:, 'sec_b'] = ranking.ranking_sec_b.apply(lambda x: x.split(':')[0].split(',')[0].split('&')[0].strip())
    ranking.loc[:, 'sec_c'] = ranking.ranking_sec_c.apply(lambda x: x.split(':')[0].split(',')[0].split('&')[0].strip())

    for e, row in ranking.iterrows():
        index = row[['ranking_a', 'ranking_b', 'ranking_c']]
        sectors = row[['sec_a', 'sec_b', 'sec_c']]
        limitA = 0.2
        limitB = 0.5
        selected_sectors = [
            (sectors_dict[f], i, f) for i,f in zip(index, sectors) 
            if i/sum(index) >= limitA and sectors_dict[f] >= limitB]
        if len(selected_sectors) > 1:
            max_index = max([i[1] for i in selected_sectors])
            selected_sectors_B = [s for s in selected_sectors if s[1]==max_index]
        ranking.loc[e, 'sec_main'] = selected_sectors_B[0][2]

    ranking.loc[ranking.sec_main == 'C', 'sec_main'] = 'CH'
    ranking.loc[ranking.sec_main == 'H', 'sec_main'] = 'CH'

    ranking.loc[ranking.sec_main == 'F', 'sec_main'] = 'F'
    ranking.loc[ranking.sec_main == 'N', 'sec_main'] = 'NMP'
    ranking.loc[ranking.sec_main == 'M', 'sec_main'] = 'NMP'
    ranking.loc[ranking.sec_main == 'P', 'sec_main'] = 'NMP'

    inx_d = ['city_name', 'hhi_city', 'hhi_city_norm', 'hhi_circ', 'hhi_circ_norm']
    inx_c = ['city_name', 'ce', 'hhi_circ_norm']
    inx_r = ['city', 'country_code', 'sec_main']

    cities = pd.merge(cities, diversity.loc[:,inx_d], left_on="city_name", right_on="city_name", how="inner")
    cities = pd.merge(cities.loc[:,inx_c], 
                      ranking.loc[:,inx_r], 
                      left_on="city_name", right_on="city", how="inner")

    data = pd.merge(cities, ghs, left_on="city_name", right_on="UC_NM_MN", how="inner")

    E_KG_dummy = pd.get_dummies(data.E_KG_NM_LST)
    E_KG_dummy = E_KG_dummy.loc[:, E_KG_dummy.sum() > 0]

    data.INCM_CMI.loc[data.INCM_CMI=='LIC'] = "LMIC"
    INCM_CMI_dummy = pd.get_dummies(data.INCM_CMI)
    INCM_CMI_dummy = INCM_CMI_dummy.loc[:, INCM_CMI_dummy.sum() > 0]

    DEV_CMI_dummy = pd.get_dummies(data.DEV_CMI)
    DEV_CMI_dummy = DEV_CMI_dummy.loc[:, DEV_CMI_dummy.sum() > 0]

    CA_dummy = pd.get_dummies(data.sec_main)
    CA_dummy = CA_dummy.loc[:, CA_dummy.sum() > 0]

    X = data.loc[:, usecol]
    X = X.join(INCM_CMI_dummy)
    testcol = X.columns

    X = X.dropna()
    X.INCM_CMI = X.INCM_CMI.cat.remove_unused_categories()
    X_num = X.loc[:, [c for c in X.columns if c not in ['sec_main', 'INCM_CMI']]]
    
    return(X, X_num)

