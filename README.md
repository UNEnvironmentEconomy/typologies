# UNEP City Typologies

This is an automatically generated document based on computation steps perform on Jupyther-Notebooks described on the following pages.

The aim of this document is to: a) describe the construction of city typologies by economic classification and main economic activity, and b) briefly describe each city type.

## Classification parameters

**Table 1.a:** Aggregated economic sectors by "alpha"-level

| Alpha | Description |
| ----- |:----------- |
| A.    | Agriculture, forestry and fishing |
| B.    | Mining and quarrying | |
| C.    | Manufacturing |
| D.    | Electricity, gas, steam and air conditioning supply |
| E.    | Water supply; sewerage, waste management and remediation activities |
| F.    | Construction |
| G.    | Wholesale and retail trade; repair of motor vehicles and motorcycles |
| H.    | Transportation and storage |
| I.    | Accommodation and food service activities |
| J.    | Information and communication |
| K.    | Financial and insurance activities |
| L.    | Real estate activities |
| M.    | Professional, scientific and technical activities |
| N.    | Administrative and support service activities |
| O.    | Public administration and defence; compulsory social security |
| P.    | Education |
| Q.    | Human health and social work activities |
| R.    | Arts, entertainment and recreation |
| S.    | Other service activities |
| T.    | Activities of households as employers; undifferentiated goods- and services-producing activities of households for own use |
| U.    | Activities of extraterritorial organizations and bodies |

**Table 1.b:** Income classes classification**

| Code | Income class                  |
| ---- | ----------------------------- |
| HIC  | High Income Countries         |
| UMIC | Upper-middle Income Countries |
| LMIC | Lower-middle Income Countries |
| LIC  | Low Income Countries          |

## Compiling a local version

```shell
$ cd typologies/
$ virtualenv --python=python3 env/
$ source env/bin/activate
$ pip install -r requirements.txt
(env)$ jupyter-book build . --builder singlehtml
```

## Compiling a pdf version

```shell
$ cd typologies/
$ virtualenv --python=python3 env/
$ source env/bin/activate
$ pip install -r requirements.txt
(env)$ jupyter-book build . --builder latexpdf
```
