# Manufacturing, transport and storage -- (CH)

## Cluster - 1 (CH-HIC)
Birmingham (GBR); Bologna (ITA); Porto (PRT); Sarnia (CAN)

[![https://upload.wikimedia.org/wikipedia/commons/b/b1/Bologna_montage_HD.jpg](https://upload.wikimedia.org/wikipedia/commons/b/b1/Bologna_montage_HD.jpg)](https://upload.wikimedia.org/wikipedia/commons/b/b1/Bologna_montage_HD.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/6/6b/Historical_part_of_Porto%2C_Portugal.jpg](https://upload.wikimedia.org/wikipedia/commons/6/6b/Historical_part_of_Porto%2C_Portugal.jpg)](https://upload.wikimedia.org/wikipedia/commons/6/6b/Historical_part_of_Porto%2C_Portugal.jpg)


**Table 5**: Desription of cluster 1

|       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|----------:|----------------:|----------:|-----------:|----------------:|
| count | 4         |       4         |      4    |       4    |        4        |
| mean  | 0.0597783 |       0.0764236 |   4505.21 |   22720.1  |        3.64538  |
| std   | 0.0165676 |       0.0124703 |   4261.83 |    8087.88 |        5.438    |
| min   | 0.0362621 |       0.0639687 |   1284.46 |   13853.8  |        0.200888 |
| 25%   | 0.0556689 |       0.0665761 |   1475.09 |   17155.6  |        0.84227  |
| 50%   | 0.0639814 |       0.0769226 |   3146.84 |   22768.6  |        1.31107  |
| 75%   | 0.0680907 |       0.0867701 |   6176.96 |   28333.1  |        4.11419  |
| max   | 0.0748885 |       0.0878804 |  10442.7  |   31489.5  |       11.7585   |

**Table 6**: Cities of cluster 1

| city       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-----------|----------:|----------------:|----------:|-----------:|----------------:|
| Birmingham | 0.0658248 |       0.0864    |   1284.46 |    31489.5 |        1.56608  |
| Bologna    | 0.0748885 |       0.0639687 |  10442.7  |    18256.2 |        0.200888 |
| Porto      | 0.0621379 |       0.0878804 |   4755.06 |    13853.8 |        1.05606  |
| Sarnia     | 0.0362621 |       0.0674452 |   1538.63 |    27281   |       11.7585   |

## Cluster - 2 (CH-UMIC)
Bucaramanga (COL); Bucharest (ROU); Buenos Aires (ARG); Durango (MEX); Ibagué (COL); Pasto (COL); Pereira (COL); Querétaro (MEX); Tampico (MEX); Tapachula (MEX); Ushuaia (ARG); Windhoek (NAM)

[![https://upload.wikimedia.org/wikipedia/commons/c/c9/Glaciar_Martial_100_2981.JPG](https://upload.wikimedia.org/wikipedia/commons/c/c9/Glaciar_Martial_100_2981.JPG)](https://upload.wikimedia.org/wikipedia/commons/c/c9/Glaciar_Martial_100_2981.JPG)
[![https://upload.wikimedia.org/wikipedia/commons/4/4b/Montaje_de_la_Ciudad_de_Buenos_Aires.png](https://upload.wikimedia.org/wikipedia/commons/4/4b/Montaje_de_la_Ciudad_de_Buenos_Aires.png)](https://upload.wikimedia.org/wikipedia/commons/4/4b/Montaje_de_la_Ciudad_de_Buenos_Aires.png)


**Table 7**: Desription of cluster 2

|       |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|-----------:|----------------:|----------:|-----------:|----------------:|
| count | 12         |      12         |     12    |      12    |      12         |
| mean  |  0.0561283 |       0.172054  |  39627.4  |   10113.3  |       0.561564  |
| std   |  0.0398031 |       0.127198  |  54734.7  |    7998.58 |       1.07251   |
| min   |  0.0106121 |       0.0488429 |   7297.88 |       0    |       0.0384137 |
| 25%   |  0.0193775 |       0.0916058 |   9536.63 |    4230.17 |       0.0905482 |
| 50%   |  0.0466061 |       0.115597  |  19274.9  |    8792.56 |       0.125052  |
| 75%   |  0.0742396 |       0.234492  |  33977.2  |   13436.2  |       0.394437  |
| max   |  0.138996  |       0.485653  | 195158    |   28720.4  |       3.82573   |

**Table 8**: Cities of cluster 2

| city         |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-------------|----------:|----------------:|----------:|-----------:|----------------:|
| Bucaramanga  | 0.0195626 |       0.288253  |  32414.5  |   14360.7  |       0.12305   |
| Bucharest    | 0.138996  |       0.0571878 |  10609.6  |   28720.4  |       0.594103  |
| Buenos Aires | 0.0106121 |       0.210636  |  10229.9  |   18763.2  |       1.09137   |
| Durango      | 0.0478392 |       0.100038  |   9706.89 |   10351.5  |       0.327882  |
| Ibagué       | 0.0704347 |       0.232064  |  94195.2  |    2775.76 |       0.0440337 |
| Pasto        | 0.106676  |       0.485653  |  32765.7  |    3170.6  |       0.0384137 |
| Pereira      | 0.0444843 |       0.241775  |  37611.4  |    6853.96 |       0.102028  |
| Querétaro    | 0.0845183 |       0.0663095 |   8573.16 |   13128    |       0.298238  |
| Tampico      | 0.0188223 |       0.102698  |   7297.88 |   11418.7  |       3.82573   |
| Tapachula    | 0.0453729 |       0.104486  |   9025.85 |    4583.36 |       0.127054  |
| Ushuaia      | 0.0154072 |       0.126707  | 195158    |       0    |       0.0561095 |
| Windhoek     | 0.0708134 |       0.0488429 |  27940.3  |    7233.61 |       0.110748  |

## Cluster - 3 (CH-LMIC)
Accra (GHA); Addis Ababa (ETH); Bahawalpur (PAK); Benguela (AGO); Bhopal (IND); Cairo (EGY); Faisalabad (PAK); Karachi (PAK); Kumasi (GHA); Lahore (PAK); Lubango (AGO); Lusaka (ZMB); Mombasa (KEN); Multan (PAK); Nakuru (KEN); Nashik (IND); Ndola (ZMB); Peshawar (PAK); Port Said (EGY); Quetta (PAK); Sargodha (PAK); Sucre (BOL); Surat (IND); Tamale (GHA); Tunis (TUN); Ulaanbaatar (MNG)

[![https://upload.wikimedia.org/wikipedia/commons/1/1b/Addis_abeba_meskele_square_%28cropped%29.jpg](https://upload.wikimedia.org/wikipedia/commons/1/1b/Addis_abeba_meskele_square_%28cropped%29.jpg)](https://upload.wikimedia.org/wikipedia/commons/1/1b/Addis_abeba_meskele_square_%28cropped%29.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/c/c9/Chinggis_Square.jpg](https://upload.wikimedia.org/wikipedia/commons/c/c9/Chinggis_Square.jpg)](https://upload.wikimedia.org/wikipedia/commons/c/c9/Chinggis_Square.jpg)


**Table 9**: Desription of cluster 3

|       |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|-----------:|----------------:|----------:|-----------:|----------------:|
| count | 26         |      26         |     26    |     26     |      26         |
| mean  |  0.0892409 |       0.156012  |  31482.6  |   4471.91  |       0.291237  |
| std   |  0.0425455 |       0.129708  |  24506.3  |   3145.06  |       0.542146  |
| min   |  0.0246389 |       0.05672   |   8916.25 |    523.327 |       0.0227523 |
| 25%   |  0.0596365 |       0.0963567 |  19582.1  |   2660     |       0.0619594 |
| 50%   |  0.088664  |       0.115978  |  25271.5  |   3317.14  |       0.114865  |
| 75%   |  0.110923  |       0.159791  |  34316.9  |   5828.36  |       0.230685  |
| max   |  0.187508  |       0.696093  | 115792    |  14246.1   |       2.70789   |

**Table 10**: Cities of cluster 3

| city        |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------------|----------:|----------------:|----------:|-----------:|----------------:|
| Accra       | 0.149251  |       0.129854  |   8916.25 |   2991.77  |       0.0879005 |
| Addis Ababa | 0.0850182 |       0.05672   |  36348.2  |   1228.99  |       0.0941175 |
| Bahawalpur  | 0.0963654 |       0.229496  |  18577.3  |   9744.59  |       0.0772363 |
| Benguela    | 0.0301094 |       0.0963054 |  24270.2  |   4429.16  |       0.0227523 |
| Bhopal      | 0.0246389 |       0.0676657 |  26181.3  |   2945.22  |       0.287924  |
| Cairo       | 0.121704  |       0.0732074 |  30942.4  |   7059.99  |       1.03513   |
| Faisalabad  | 0.0865705 |       0.128828  |  24361.7  |   3427.24  |       0.231885  |
| Karachi     | 0.1197    |       0.157366  |  39044.9  |   4270.32  |       0.309081  |
| Kumasi      | 0.154535  |       0.196316  |  11011.1  |   2934.62  |       0.0748011 |
| Lahore      | 0.154192  |       0.108867  |  30336    |   3207.05  |       0.227083  |
| Lubango     | 0.0288643 |       0.112398  |  21720.7  |   5534.65  |       0.0304462 |
| Lusaka      | 0.0806541 |       0.188672  |  19393.5  |   2579.2   |       0.0891405 |
| Mombasa     | 0.0588712 |       0.0965106 |  24081    |   2902.41  |       0.135832  |
| Multan      | 0.0868594 |       0.110774  |  20147.8  |   3638.57  |       0.466     |
| Nakuru      | 0.0619324 |       0.160599  |  52928.9  |    523.327 |       0.0337122 |
| Nashik      | 0.0341101 |       0.196869  |  34402.8  |   6526.8   |       0.198352  |
| Ndola       | 0.0976697 |       0.153545  |  11065.1  |   1953.54  |       0.0595103 |
| Peshawar    | 0.10425   |       0.0886097 |  34059.2  |   2014.1   |       0.195418  |
| Port Said   | 0.091656  |       0.0724612 |  29144.6  |   9883.34  |       0.135613  |
| Quetta      | 0.0534448 |       0.118366  |  22791.6  |   2434.31  |       0.053263  |
| Sargodha    | 0.077828  |       0.11109   |  36586.5  |   4367.55  |       0.0693066 |
| Sucre       | 0.113147  |       0.696093  | 115792    |   1016.05  |       0.0444031 |
| Surat       | 0.0343475 |       0.390691  |  97146.7  |   5926.27  |       2.70789   |
| Tamale      | 0.187508  |       0.132006  |  12279.9  |   3198.04  |       0.0337439 |
| Tunis       | 0.0965663 |       0.113591  |   9653.78 |   7286.57  |       0.66059   |
| Ulaanbaatar | 0.0904687 |       0.069408  |  27364.2  |  14246.1   |       0.211032  |

