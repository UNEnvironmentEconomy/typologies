# Construction -- (F)

## Cluster - 10 (F-HIC)




## Cluster - 11 (F-UMIC)
Colima (MEX); Trelew (ARG)

[![https://upload.wikimedia.org/wikipedia/commons/8/8d/BancoNaci%C3%B3n_Trelew2008.JPG](https://upload.wikimedia.org/wikipedia/commons/8/8d/BancoNaci%C3%B3n_Trelew2008.JPG)](https://upload.wikimedia.org/wikipedia/commons/8/8d/BancoNaci%C3%B3n_Trelew2008.JPG)
[![https://upload.wikimedia.org/wikipedia/commons/2/29/CeballosHotelColima.jpg](https://upload.wikimedia.org/wikipedia/commons/2/29/CeballosHotelColima.jpg)](https://upload.wikimedia.org/wikipedia/commons/2/29/CeballosHotelColima.jpg)


**Table 23**: Desription of cluster 11

|       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|----------:|----------------:|----------:|-----------:|----------------:|
| count | 2         |       2         |      2    |       2    |       2         |
| mean  | 0.0618728 |       0.104975  |  10532.8  |   15131.9  |       0.207386  |
| std   | 0.0149232 |       0.0159656 |   1521.17 |    6957.65 |       0.0892625 |
| min   | 0.0513205 |       0.0936854 |   9457.17 |   10212.1  |       0.144267  |
| 25%   | 0.0565966 |       0.0993301 |   9994.99 |   12672    |       0.175827  |
| 50%   | 0.0618728 |       0.104975  |  10532.8  |   15131.9  |       0.207386  |
| 75%   | 0.0671489 |       0.11062   |  11070.6  |   17591.8  |       0.238945  |
| max   | 0.0724251 |       0.116264  |  11608.4  |   20051.7  |       0.270504  |

**Table 24**: Cities of cluster 11

| city   |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-------|----------:|----------------:|----------:|-----------:|----------------:|
| Colima | 0.0724251 |       0.0936854 |   9457.17 |    10212.1 |        0.270504 |
| Trelew | 0.0513205 |       0.116264  |  11608.4  |    20051.7 |        0.144267 |

## Cluster - 12 (F-LMIC)
Cochabamba (BOL); Jaipur (IND); Potosí (BOL); Tarija (BOL); Trinidad (BOL)

[![https://upload.wikimedia.org/wikipedia/commons/0/02/Cadredal_metrpolitana_cochabamba_2019_%281%29.jpg](https://upload.wikimedia.org/wikipedia/commons/0/02/Cadredal_metrpolitana_cochabamba_2019_%281%29.jpg)](https://upload.wikimedia.org/wikipedia/commons/0/02/Cadredal_metrpolitana_cochabamba_2019_%281%29.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/1/1b/Jaipur_Montage.png](https://upload.wikimedia.org/wikipedia/commons/1/1b/Jaipur_Montage.png)](https://upload.wikimedia.org/wikipedia/commons/1/1b/Jaipur_Montage.png)


**Table 25**: Desription of cluster 12

|       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|----------:|----------------:|----------:|-----------:|----------------:|
| count | 5         |        5        |       5   |       5    |       5         |
| mean  | 0.12629   |        0.576088 |   33745.7 |    4241.03 |       0.126099  |
| std   | 0.0290797 |        0.291043 |   24167.4 |    2468.17 |       0.111988  |
| min   | 0.0983872 |        0.127656 |   13989.5 |    2142.39 |       0.0322016 |
| 25%   | 0.100925  |        0.510383 |   14173.5 |    3285.98 |       0.0387982 |
| 50%   | 0.118324  |        0.639692 |   21364.8 |    3345.1  |       0.0666731 |
| 75%   | 0.153582  |        0.683279 |   53573   |    3930.17 |       0.221449  |
| max   | 0.160234  |        0.919427 |   65627.8 |    8501.51 |       0.271373  |

**Table 26**: Cities of cluster 12

| city       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-----------|----------:|----------------:|----------:|-----------:|----------------:|
| Cochabamba | 0.153582  |        0.639692 |   13989.5 |    3930.17 |       0.221449  |
| Jaipur     | 0.118324  |        0.127656 |   21364.8 |    3345.1  |       0.271373  |
| Potosí     | 0.0983872 |        0.919427 |   53573   |    3285.98 |       0.0387982 |
| Tarija     | 0.160234  |        0.683279 |   65627.8 |    8501.51 |       0.0666731 |
| Trinidad   | 0.100925  |        0.510383 |   14173.5 |    2142.39 |       0.0322016 |

