# Wholesale and retail trade; repair of motor vehicles and motorcycles -- (G)

## Cluster - 4 (G-HIC)
Barrie (CAN); Belleville (CAN); Calgary (CAN); Edmonton (CAN); Halifax (CAN); Hamilton (CAN); Helsinki (FIN); Heraklion (GRC); Lethbridge (CAN); Moncton (CAN); Nanaimo (CAN); Oshawa (CAN); Red Deer (CAN); Regina (CAN); Saskatoon (CAN); Sherbrooke (CAN); Tijuana (MEX); Toronto (CAN); Vancouver (CAN); Winnipeg (CAN)

[![https://upload.wikimedia.org/wikipedia/commons/6/68/Edmonton_Montage_2020.jpg](https://upload.wikimedia.org/wikipedia/commons/6/68/Edmonton_Montage_2020.jpg)](https://upload.wikimedia.org/wikipedia/commons/6/68/Edmonton_Montage_2020.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/6/65/Toronto_Skyline_Summer_2020.jpg](https://upload.wikimedia.org/wikipedia/commons/6/65/Toronto_Skyline_Summer_2020.jpg)](https://upload.wikimedia.org/wikipedia/commons/6/65/Toronto_Skyline_Summer_2020.jpg)


**Table 11**: Desription of cluster 4

|       |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|-----------:|----------------:|----------:|-----------:|----------------:|
| count | 20         |      20         |     20    |      20    |       20        |
| mean  |  0.0609516 |       0.0921196 |   3649.67 |   23397.5  |        3.04435  |
| std   |  0.0182582 |       0.0540952 |   1461.84 |    5657.88 |        2.27409  |
| min   |  0.0286133 |       0.050643  |   1721.47 |   14273.8  |        0.260827 |
| 25%   |  0.0456037 |       0.060751  |   2994.98 |   19969.1  |        1.15679  |
| 50%   |  0.0610341 |       0.0848251 |   3273.04 |   22494.5  |        2.11075  |
| 75%   |  0.0725686 |       0.0916992 |   4101.04 |   27912.6  |        4.95662  |
| max   |  0.0911997 |       0.293365  |   7752.06 |   34650.1  |        7.92208  |

**Table 12**: Cities of cluster 4

| city       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-----------|----------:|----------------:|----------:|-----------:|----------------:|
| Barrie     | 0.0631177 |       0.0908325 |   3257.98 |    23337.8 |        1.68458  |
| Belleville | 0.0378732 |       0.0914155 |   1721.47 |    34650.1 |        2.91705  |
| Calgary    | 0.0678834 |       0.0514883 |   3384.79 |    30299.4 |        7.92208  |
| Edmonton   | 0.0711947 |       0.0625079 |   2924.73 |    28849.4 |        6.20214  |
| Halifax    | 0.0706211 |       0.114859  |   4101    |    20005.3 |        4.94342  |
| Hamilton   | 0.0647743 |       0.089859  |   5235.59 |    14955.2 |        1.17701  |
| Helsinki   | 0.0911997 |       0.0679117 |   7752.06 |    18877.2 |        1.0083   |
| Heraklion  | 0.087538  |       0.0703367 |   6488.25 |    14649.8 |        0.260827 |
| Lethbridge | 0.0443718 |       0.132592  |   2160.72 |    28579.5 |        1.02433  |
| Moncton    | 0.0529161 |       0.0848667 |   3018.39 |    14273.8 |        0.98985  |
| Nanaimo    | 0.0417037 |       0.0546037 |   2267.34 |    21583.3 |        1.46313  |
| Oshawa     | 0.0589505 |       0.050643  |   3024.84 |    27140.7 |        4.17303  |
| Red Deer   | 0.0374492 |       0.0925502 |   2397.4  |    25790.8 |        1.57478  |
| Regina     | 0.0524569 |       0.0847836 |   3026.2  |    22300.8 |        4.99622  |
| Saskatoon  | 0.0579495 |       0.0685205 |   3290.68 |    21963.4 |        2.53693  |
| Sherbrooke | 0.0460143 |       0.144098  |   3132.96 |    19860.7 |        1.09612  |
| Tijuana    | 0.0286133 |       0.0882756 |   3824.18 |    30226.5 |        1.50166  |
| Toronto    | 0.0842522 |       0.0554802 |   4101.15 |    27690.3 |        5.17011  |
| Vancouver  | 0.083462  |       0.053404  |   4595.57 |    22688.1 |        6.85559  |
| Winnipeg   | 0.0766902 |       0.293365  |   3288.1  |    20228.7 |        3.38994  |

## Cluster - 5 (G-UMIC)
Aguascalientes (MEX); Babahoyo (ECU); Barranquilla (COL); Cali (COL); Campeche (MEX); Coatzacoalcos (MEX); Córdoba (ARG); Cúcuta (COL); Cuenca (ECU); Cuernavaca (MEX); Culiacán (MEX); Guadalajara (MEX); Guaranda (ECU); Hermosillo (MEX); Ibarra (ECU); Kingston (CAN); Medellín (COL); Mérida (MEX); Mexicali (MEX); Montería (COL); Monterrey (MEX); Morelia (MEX); Oaxaca (MEX); Pachuca (MEX); Puebla (MEX); Río Cuarto (ARG); Río Gallegos (ARG); Salta (ARG); Saltillo (MEX); San Luis Potosí (MEX); Split (HRV); Tepic (MEX); Tlaxcala (MEX); Toluca (MEX); Villahermosa (MEX); Villavicencio (COL)

[![https://upload.wikimedia.org/wikipedia/commons/e/e1/Collage_Monterrey.jpg](https://upload.wikimedia.org/wikipedia/commons/e/e1/Collage_Monterrey.jpg)](https://upload.wikimedia.org/wikipedia/commons/e/e1/Collage_Monterrey.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/f/f5/Metro_de_Medell%C3%ADn%2C_Colombia.jpg](https://upload.wikimedia.org/wikipedia/commons/f/f5/Metro_de_Medell%C3%ADn%2C_Colombia.jpg)](https://upload.wikimedia.org/wikipedia/commons/f/f5/Metro_de_Medell%C3%ADn%2C_Colombia.jpg)


**Table 13**: Desription of cluster 5

|       |          ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|------------:|----------------:|----------:|-----------:|----------------:|
| count | 36          |      36         |     36    |      36    |       36        |
| mean  |  0.0518319  |       0.128562  |  24612.4  |   12461.1  |        0.385263 |
| std   |  0.0273903  |       0.142568  |  50669.8  |   12977.4  |        0.316706 |
| min   |  0.00269469 |       0.0447117 |   4732.94 |    2137.97 |        0.012817 |
| 25%   |  0.0288527  |       0.0729167 |   8654.12 |    7408.45 |        0.193131 |
| 50%   |  0.0538154  |       0.0966962 |  10149.7  |    9422.72 |        0.272021 |
| 75%   |  0.0693626  |       0.118511  |  17962.3  |   12380.9  |        0.516881 |
| max   |  0.107      |       0.818443  | 288954    |   79964.9  |        1.45866  |

**Table 14**: Cities of cluster 5

| city            |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:----------------|-----------:|----------------:|----------:|-----------:|----------------:|
| Aguascalientes  | 0.0463182  |       0.0570573 |   9807.01 |   12454.7  |       0.24069   |
| Babahoyo        | 0.00269469 |       0.1589    |  32286.5  |    7356.53 |       0.0231509 |
| Barranquilla    | 0.00391161 |       0.131696  |  19985    |    7698.97 |       0.19955   |
| Cali            | 0.0389233  |       0.0532921 |  26268.9  |   11083.3  |       0.253877  |
| Campeche        | 0.0661472  |       0.105546  |   7793.82 |   79964.9  |       0.173874  |
| Coatzacoalcos   | 0.0874666  |       0.121423  |   6409.64 |    9768.28 |       0.57115   |
| Córdoba         | 0.0227536  |       0.0693865 |  10635.8  |    7180.14 |       0.33821   |
| Cúcuta          | 0.0178272  |       0.514893  |  15663.1  |    4313.75 |       0.217054  |
| Cuenca          | 0.0340913  |       0.0964833 |  26632    |    8557.84 |       0.0816077 |
| Cuernavaca      | 0.052349   |       0.103729  |  10060.9  |    8969.87 |       0.316932  |
| Culiacán        | 0.0504695  |       0.0871405 |  11263.3  |    9876.42 |       0.213118  |
| Guadalajara     | 0.0605147  |       0.0843906 |  12908.4  |   12356.3  |       0.981195  |
| Guaranda        | 0.00605254 |       0.0979423 | 288954    |    2137.97 |       0.012817  |
| Hermosillo      | 0.0719427  |       0.0613488 |   8041.55 |   12696.2  |       0.312293  |
| Ibarra          | 0.0288515  |       0.0987769 | 141099    |    4328.76 |       0.147782  |
| Kingston        | 0.0552818  |       0.0766654 |   7717.82 |    5690.38 |       0.136048  |
| Medellín        | 0.0734969  |       0.0969091 |  37994.1  |   10064.3  |       0.234906  |
| Mérida          | 0.0690617  |       0.0867357 |   6058.29 |    9872.09 |       0.653751  |
| Mexicali        | 0.0656087  |       0.109814  |  15547.6  |   11891.1  |       0.434515  |
| Montería        | 0.0984587  |       0.25423   |  17288.1  |    4749.85 |       0.0697714 |
| Monterrey       | 0.0466986  |       0.0520779 |   9791.84 |   22500.5  |       0.806795  |
| Morelia         | 0.0716922  |       0.0882543 |   8858.31 |    7487.13 |       0.222654  |
| Oaxaca          | 0.0702655  |       0.11754   |   9163.83 |    5870.76 |       0.402086  |
| Pachuca         | 0.103192   |       0.100195  |   8867.53 |    8114.76 |       0.55988   |
| Puebla          | 0.0580755  |       0.0740934 |   7932.61 |    7713.57 |       1.04467   |
| Río Cuarto      | 0.0286958  |       0.128051  |   6168    |   14565.2  |       0.345122  |
| Río Gallegos    | 0.00806864 |       0.144276  |  21883    |   31554.8  |       0.215753  |
| Salta           | 0.0286389  |       0.0776184 |  16483.3  |    8071.65 |       0.229313  |
| Saltillo        | 0.107      |       0.0447117 |   8987.3  |   13406.7  |       0.498404  |
| San Luis Potosí | 0.0621195  |       0.0561053 |   9783.67 |   10492.5  |       0.502548  |
| Split           | 0.0806536  |       0.0554947 |   6203.22 |   10872.5  |       1.45866   |
| Tepic           | 0.0679235  |       0.109012  |  10771.2  |    7425.76 |       0.137871  |
| Tlaxcala        | 0.028853   |       0.0674143 |   4732.94 |    7024.99 |       0.817286  |
| Toluca          | 0.0484508  |       0.134543  |  10238.5  |    9077.16 |       0.581158  |
| Villahermosa    | 0.0590082  |       0.0940532 |   9449.39 |   24648.5  |       0.290166  |
| Villavicencio   | 0.0443932  |       0.818443  |  24316    |   18762.2  |       0.144804  |

## Cluster - 6 (G-LMIC)
Agra (IND); Ahmedabad (IND); Cobija (BOL); Erdenet (MNG); Freetown (SLE); Gujranwala (PAK); Indore (IND); Kampala (UGA); Kisumu (KEN); Kitwe (ZMB); Luanda (AGO); Meerut (IND); Oruro (BOL); Sukkur (PAK)

[![https://upload.wikimedia.org/wikipedia/commons/9/97/Freetown_arial_view_%283%29.jpg](https://upload.wikimedia.org/wikipedia/commons/9/97/Freetown_arial_view_%283%29.jpg)](https://upload.wikimedia.org/wikipedia/commons/9/97/Freetown_arial_view_%283%29.jpg)
[![https://upload.wikimedia.org/wikipedia/commons/3/3a/Collage_Cobija.png](https://upload.wikimedia.org/wikipedia/commons/3/3a/Collage_Cobija.png)](https://upload.wikimedia.org/wikipedia/commons/3/3a/Collage_Cobija.png)


**Table 15**: Desription of cluster 6

|       |         ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:------|-----------:|----------------:|----------:|-----------:|----------------:|
| count | 14         |      14         |      14   |     14     |      14         |
| mean  |  0.0823287 |       0.244767  |   28498.7 |   4901.8   |       0.15378   |
| std   |  0.0561483 |       0.236421  |   15115.3 |   5827.04  |       0.133702  |
| min   |  0.0106608 |       0.0713228 |   10379.7 |    790.853 |       0.0288937 |
| 25%   |  0.045645  |       0.111274  |   15354.9 |   2384.87  |       0.0528013 |
| 50%   |  0.058552  |       0.14113   |   27773.4 |   3003.38  |       0.0770236 |
| 75%   |  0.109919  |       0.25106   |   35323.8 |   5912.77  |       0.265914  |
| max   |  0.201683  |       0.788641  |   63603.8 |  24019.2   |       0.389957  |

**Table 16**: Cities of cluster 6

| city       |        ce |   hhi_circ_norm |   P15_B15 |   GDP15_PP |   E_EC2E_I15_PP |
|:-----------|----------:|----------------:|----------:|-----------:|----------------:|
| Agra       | 0.172682  |       0.14716   |   35728.8 |   2601.74  |       0.26897   |
| Ahmedabad  | 0.0526557 |       0.134306  |   36040.6 |   6422.53  |       0.364011  |
| Cobija     | 0.0680727 |       0.762683  |   10379.7 |   5996.81  |       0.0583359 |
| Erdenet    | 0.0430466 |       0.0713228 |   27648.2 |  24019.2   |       0.076746  |
| Freetown   | 0.112375  |       0.265065  |   25300.7 |   1137.65  |       0.0509565 |
| Gujranwala | 0.0445796 |       0.143363  |   34108.8 |   3129.82  |       0.317565  |
| Indore     | 0.0106608 |       0.343893  |   27898.6 |   2844.68  |       0.256749  |
| Kampala    | 0.0389895 |       0.108531  |   17507.1 |   1229.14  |       0.0404223 |
| Kisumu     | 0.0644484 |       0.138897  |   63603.8 |    790.853 |       0.0288937 |
| Kitwe      | 0.10255   |       0.111944  |   10980.2 |   2312.57  |       0.0395684 |
| Luanda     | 0.0492029 |       0.11105   |   14637.4 |   6180.14  |       0.114721  |
| Meerut     | 0.0488413 |       0.209044  |   33192.4 |   2876.93  |       0.389957  |
| Oruro      | 0.201683  |       0.788641  |   13608.3 |   5660.68  |       0.0773012 |
| Sukkur     | 0.142815  |       0.0908347 |   48346.5 |   3422.49  |       0.0687195 |

